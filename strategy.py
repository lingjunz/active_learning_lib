

import torch
import numpy as np
import torchvision.transforms as T
from keras.datasets import cifar10
import pickle
import tqdm
from collections import defaultdict
from scipy.stats import gaussian_kde
from torch.utils.data import DataLoader
from data.UserDataset import UserDataset
from data_utils import make_train_loader
from profileNN import profileDNN

# model_weights = {
#     "resnet18_LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_1.pth",
#     "resnet18_Entropy":"./cifar10/train/weights/active_resnet18_cifar10_Entropy_1.pth",
# }


def get_sorted_sequence(strategy,models,unlabeled_loader,model_name=None):
    if strategy == "LLAL":
        # Measure uncertainty of each data points in the subset
        scores = get_uncertainty(models, unlabeled_loader)
    elif strategy == "Random":
        arg = np.random.choice(50000,50000,replace=False)
        arg = torch.Tensor(arg).int()
        # np.random.shuffle(arg)
        return arg
    elif strategy == "LeastConfidence":
        scores = get_LC_score(models,unlabeled_loader)
    elif strategy == "Margin":
        scores = get_Margin_score(models,unlabeled_loader)
    elif strategy == "Entropy":
        scores = get_Entropy_score(models,unlabeled_loader)
        scores = torch.Tensor(scores)
    elif strategy == "GINI":
        scores = get_GINI_score(models,unlabeled_loader)
    elif strategy == "XENT":
        scores = get_XENT_score(models,unlabeled_loader)
        scores = torch.Tensor(scores)
    elif strategy == "VRO":
        scores = get_VRO_score(models,unlabeled_loader)
        scores = torch.Tensor(scores)
    elif strategy == "LSA":
        scores = get_LSA_scores(models,unlabeled_loader,model_name=model_name)
        scores = torch.Tensor(scores)
    elif strategy == "DSA":
        scores = get_DSA_scores(models,unlabeled_loader,model_name=model_name)
        scores = torch.Tensor(scores)
    
    arg = np.argsort(scores)
    return arg



def select_meta_info(model_name,net,selected_layers,std_threshold,phase):

    if phase == "train":
        train_idx = {
            "resnet18_LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_temp_idx.npy",
            "resnet18_LSA":"./cifar10/train/weights/active_resnet18_cifar10_LSA_temp_idx.npy",
            "resnet18_DSA":"./cifar10/train/weights/active_resnet18_cifar10_DSA_temp_idx.npy",
        }
        train_loader,_,_ = make_train_loader(train_idx[model_name])
        profiling_dic = profileDNN(net,train_loader)

    else:
        profiling_dic = {
            "resnet18_Entropy":"./cifar10/profiling/resnet18_Entropy_0_10000.pickle",
            "resnet18_LLAL": "./cifar10/profiling/resnet18_LLAL_0_10000.pickle"
        }
        profiling_dic = pickle.load(open(profiling_dic[model_name],"rb"))

    temp = torch.Tensor(np.random.randint(0,256,(2,3,32,32))/255.).cuda()
    _, feature_list, name_list = net.feature_list(temp)
    selected_idx = [i for i,layer_name in enumerate(name_list) if layer_name in selected_layers]
    feature_nums = [feature_list[idx].shape[1] for idx in selected_idx]
    neurons_id = []
    for i,idx in enumerate(selected_idx):
        cur_neurons = []
        for neuron_id in range(feature_nums[i]):
            key = (name_list[idx], neuron_id)
            if profiling_dic[key][2] > std_threshold:
                cur_neurons.append(neuron_id)
        neurons_id.append(np.array(cur_neurons))
    return selected_idx, neurons_id

def extract_features(net, dataloader, selected_idx, neurons_id):
    features = []
    to_np = lambda x: x.data.cpu().numpy()
    preds = []
    with torch.no_grad():
        for (samples,labels) in dataloader:
            samples,labels = samples.cuda(),labels.cuda()
            cur_features = []
            logits, feature_list, _ = net.feature_list(samples)
            _, cur_preds = torch.max(logits.data,1)
            preds.append(to_np(cur_preds))
            for i,layer_idx in enumerate(selected_idx):
                selected_neurons = neurons_id[i]
                layer_features = feature_list[layer_idx]
                if len(layer_features.shape) == 4:
                    layer_features = to_np(layer_features).mean(axis=(2,3))
                else:
                    layer_features = to_np(layer_features)
                    assert len(layer_features.shape) == 2, layer_features.shape
#                 print(selected_neurons,selected_neurons.shape,layer_features.shape)
                cur_features.append(layer_features[:,selected_neurons])
            cur_features = np.concatenate(cur_features, axis=1)
            features.append(cur_features)
    features = np.concatenate(features,axis=0)
    print("Extract features:{}".format(features.shape))
    return features,np.concatenate(preds,axis=0)

def get_train_features(net, model_name, selected_idx, neurons_id, phase):
    if phase == "train":
        train_idx = {
            "resnet18_LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_temp_idx.npy",
            "resnet18_LSA":"./cifar10/train/weights/active_resnet18_cifar10_LSA_temp_idx.npy",
            "resnet18_DSA":"./cifar10/train/weights/active_resnet18_cifar10_DSA_temp_idx.npy",
        }
    else:
        train_idx = {
            "resnet18_LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_1_idx.npy",
        }
    num_classes = 10
    
    train_loader,idx,y_train = make_train_loader(train_idx[model_name])
    train_features, train_preds = extract_features(net, train_loader, selected_idx, neurons_id)
    train_idx = np.where(train_preds==y_train)[0]
    print("Correct for training data:{}/{}".format(len(train_idx),len(idx)))
    train_features, train_preds = train_features[train_idx], train_preds[train_idx]
    return train_features, train_preds


def get_LSA_scores(models,unlabeled_loader,model_name,std_threshold=0.01,phase="train"):
    num_classes = 10
    net = models['backbone']
    net.eval()
    selected_idx, neurons_id = select_meta_info(model_name,net,['layer_4_1_1','logits'],std_threshold,phase)
    
    train_features, train_preds = get_train_features(net, model_name, selected_idx, neurons_id, phase)
    kdes = defaultdict()
    for label in range(num_classes):
        cur_idx = np.where(train_preds==label)[0]
        print(">>> Train num for label {}: {}".format(label,len(cur_idx)))
        refined_ats = np.transpose(train_features[cur_idx])
        if refined_ats.shape[0] == 0:
            assert False,"ats were removed by threshold {}:{}".format(std_threshold,refined_ats.shape)
        kdes[label] = gaussian_kde(refined_ats)
        
    untrain_features,untrain_preds = extract_features(net, unlabeled_loader, selected_idx, neurons_id)
    
    lsa_scores = np.zeros_like(untrain_preds)
    for i in range(num_classes):
        kde = kdes[i]
        cur_idx = np.where(untrain_preds==i)[0]
        cur_lsa = -kde.logpdf(np.transpose(untrain_features[cur_idx]))
        lsa_scores[cur_idx] = cur_lsa
    
    return lsa_scores


def find_closest_at(at,train_ats):
    dist = np.linalg.norm(at - train_ats, axis=1)
    return (min(dist), train_ats[np.argmin(dist)])

def get_DSA_scores(models,unlabeled_loader,model_name,std_threshold=0.01,phase="train"):
    num_classes = 10
    net = models['backbone']
    net.eval()
    selected_idx, neurons_id = select_meta_info(model_name,net,['layer_4_1_1','logits'],std_threshold,phase)
    train_features, train_preds = get_train_features(net, model_name, selected_idx, neurons_id,phase)
    untrain_features,untrain_preds = extract_features(net, unlabeled_loader, selected_idx, neurons_id)

    dsa_scores = []
    class_idx_dic = defaultdict()
    for i in range(num_classes):
        class_idx_dic[i] = np.where(train_preds==i)[0]
        
    for i, at in enumerate(tqdm.tqdm(untrain_features)):
        label = untrain_preds[i]
        a_dist, a_dot = find_closest_at(
            at, train_features[class_idx_dic[label]]
        )
        b_dist, _ = find_closest_at(
            a_dot, train_features[list(set(range(len(train_preds))) - set(class_idx_dic[label]))]
        )
        dsa_scores.append(a_dist / b_dist)
    
    return np.array(dsa_scores)


def cal_accuracy(models,unlabeled_loader,type='backbone'):
    model = models[type]
    model.eval()
    _preds = []
    _pred_vectors = []
    correct = 0
    to_np = lambda x: x.data.cpu().numpy()
    concat = lambda x: np.concatenate(x,axis=0)
    with torch.no_grad():
        for (inputs, labels) in unlabeled_loader:
            inputs = inputs.cuda()
            labels = labels.cuda()
            logits, _ = model(inputs)
            pred_vectors = torch.softmax(logits.data, 1)
            msps, preds = torch.max(pred_vectors, 1)
            correct += (preds == labels).sum().item()
            _pred_vectors.append(to_np(pred_vectors))
            _preds.append(to_np(preds))
            
    total_num = len(unlabeled_loader.sampler)
    acc = correct / total_num
    print("Acc for {}:{},dataset size:{}".format(type,acc,total_num))
    return concat(_preds),concat(_pred_vectors),acc
    

def get_VRO_score(models,unlabeled_loader,repeat_num = 50):
    orig_preds,orig_pred_vectors,_ = cal_accuracy(models,unlabeled_loader,type='backbone')
    counter = np.zeros(len(unlabeled_loader.sampler)) # sample_num

    for i in range(repeat_num):
        preds,pred_vectors,_ = cal_accuracy(models,unlabeled_loader,type='dropout')
        counter += preds == orig_preds
    VRO = 1-counter/repeat_num   # sample_num
    return VRO





def get_XENT_score(models,unlabeled_loader):
    model = models['backbone']
    model.eval()
    _scores = []
    to_np = lambda x: x.data.cpu().numpy()
    with torch.no_grad():
        for (inputs, labels) in unlabeled_loader:
            inputs = inputs.cuda()
            logits, _ = model(inputs)
            scores = to_np((logits.mean(1) - torch.logsumexp(logits,dim=1)))
            _scores.append(scores)
    _scores = np.concatenate(_scores,axis=0)

    return _scores





def get_Entropy_score(models,unlabeled_loader):
    from scipy.stats import entropy
    model = models['backbone']
    model.eval()
    _scores = []
    to_np = lambda x: x.data.cpu().numpy()
    with torch.no_grad():
        for (inputs, labels) in unlabeled_loader:
            inputs = inputs.cuda()
            logits, _ = model(inputs)
            pred_vectors = torch.softmax(logits.data,1)
            pred_vectors = to_np(pred_vectors)
            scores = entropy(pred_vectors,axis=1)
            _scores.append(scores)
    _scores = np.concatenate(_scores,axis=0)
    return _scores

def get_GINI_score(models,unlabeled_loader):
    model = models['backbone']
    model.eval()
    scores = torch.Tensor([]).cuda()
    with torch.no_grad():
        for (inputs, labels) in unlabeled_loader:
            inputs = inputs.cuda()
            logits, _ = model(inputs)
            pred_vectors = torch.softmax(logits.data,1)
            cur_scores =  1 - torch.sum(pred_vectors**2,1) # 1 -
            scores = torch.cat((scores, cur_scores), 0)

    return scores.cpu()


def get_Margin_score(models,unlabeled_loader):
    model = models['backbone']
    model.eval()
    scores = torch.Tensor([]).cuda()
    with torch.no_grad():
        for (inputs, labels) in unlabeled_loader:
            inputs = inputs.cuda()
            logits, _ = model(inputs)
            pred_vectors = torch.softmax(logits.data,1)
            values, _ = torch.sort(pred_vectors,1)
            cur_scores = -1*(values[:,-1]-values[:,-2])
            scores = torch.cat((scores, cur_scores), 0)

    return scores.cpu()


def get_LC_score(models,unlabeled_loader):
    model = models['backbone']
    model.eval()
    scores = torch.Tensor([]).cuda()
    with torch.no_grad():
        for (inputs, labels) in unlabeled_loader:
            inputs = inputs.cuda()
            logits, _ = model(inputs)
            msps, preds = torch.max(torch.softmax(logits.data,1),1)
            scores = torch.cat((scores, 1-msps), 0)
    return scores.cpu()



def get_uncertainty(models, unlabeled_loader):
    models['backbone'].eval()
    models['module'].eval()
    uncertainty = torch.Tensor([]).cuda()
    with torch.no_grad():
        for (inputs, labels) in unlabeled_loader:
            inputs = inputs.cuda()
            # labels = labels.cuda()
            scores, features = models['backbone'](inputs)
            pred_loss = models['module'](features) # pred_loss = criterion(scores, labels) # ground truth loss
            pred_loss = pred_loss.view(pred_loss.size(0))
            uncertainty = torch.cat((uncertainty, pred_loss), 0)
    
    return uncertainty.cpu()