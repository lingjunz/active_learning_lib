from matplotlib import pyplot as plt
# %matplotlib inline
import seaborn as  sns
import joblib
import pandas as pd
import os
import numpy as np

# Train Phase
'''
folder = "cifar10/train/weights"
idx_files = np.sort([item for item in os.listdir(folder) if item.split(".")[-1]=="dic" and item.split("_")[1]=="resnet18"])
strategies = np.sort([item.split('_')[3] for item in idx_files])
idx_files,strategies
'''
def extract_record(cur_dic,dataset,metric): # datasets: train, val, test
    cur_record = []
    for i in range(10):
        cur_epoch = "epoch_{}".format(i)
        cur_record.append(cur_dic[cur_epoch][dataset][metric])
    return cur_record

def extract_all_records(folder,dataset,metric,model_name):
    dic_files = np.sort([item for item in os.listdir(folder) if item.split(".")[-1]=="dic" and item.split("_")[1]==model_name])
    strategies = []
    records = []
    for filename in dic_files:
        cur_file = os.path.join(folder,filename)
        strategies.append(cur_file.split("_")[3])
        cur_dic = joblib.load(cur_file)
        records.append(extract_record(cur_dic,dataset,metric))        
    return strategies,records

folder = "cifar10/train/weights"
dataset = "test"
model_name = "resnet18"
metric  = 2

strategies,records = extract_all_records(folder,dataset,metric,model_name)
df = pd.DataFrame(np.array(records).transpose())
df.columns = strategies
# df.index  = [i for i in range(12)]
df = df.transpose()
df

plt.figure(figsize=(10,10))
df.transpose().plot(marker='.')
plt.show()

# Test Phase

folder = "cifar10/train/retrain"
idx_files = np.sort([item for item in os.listdir(folder) if item.split("_")[-2]=="m" and item.split("_")[1]=="resnet18"])
strategies = np.sort([item.split('_')[3] for item in idx_files])
idx_files,strategies

cal_sim = lambda a,b:len(set(a).intersection(set(b)))/len(set(a).union(set(b)))
heatmap = np.ones((len(strategies),len(strategies)))*100
heatmap

all_sequences = []
for i in range(len(strategies)):
    cur = np.load(os.path.join(folder,idx_files[i]))[:10000]
    all_sequences.append(cur)


    
for i in range(len(strategies)):
    for j in range(i+1,len(strategies)):
        cur_sim = np.round(cal_sim(all_sequences[i],all_sequences[j])*100,2)
        print("Similarity between {} and {}:{}".format(strategies[i],strategies[j],cur_sim))
        heatmap[i][j] = heatmap[j][i] = cur_sim

heatmap = pd.DataFrame(heatmap)
heatmap.columns = strategies
heatmap.index = strategies
heatmap

# sns.set_palette(sns.palplot(sns.color_palette("Reds")))
f,ax= plt.subplots(figsize=(8,8))
sns.heatmap(heatmap, annot=True, fmt=".2f",linewidths=0.5,ax=ax,cmap='Reds',vmin=0,vmax=100)
label_y = ax.get_yticklabels()
plt.setp(label_y, rotation=360, horizontalalignment='right')
label_x = ax.get_xticklabels()
plt.setp(label_x, rotation=45, horizontalalignment='right')

plt.show()

def extract_record(cur_dic,dataset,metric): # datasets: train, val, test
    cur_record = []
    for i in range(12):
        cur_epoch = "epoch_{}".format(i)
        cur_record.append(cur_dic[cur_epoch][dataset][metric])
    return cur_record

def extract_all_records(folder,dataset,metric,model_name):
    dic_files = [item for item in os.listdir(folder) if item.split("_")[-1]=="a.dic" and item.split("_")[1]==model_name]
    strategies = []
    records = []
    for filename in dic_files:
        cur_file = os.path.join(folder,filename)
        strategies.append(cur_file.split("_")[3])
        cur_dic = joblib.load(cur_file)
        records.append(extract_record(cur_dic,dataset,metric))        
    return strategies,records

folder = "cifar10/train/retrain"
dataset = "train"
model_name = "resnet18"
metric  = 0

strategies,records = extract_all_records(folder,dataset,metric,model_name)
df = pd.DataFrame(np.array(records).transpose())
df.columns = strategies
df.index  = [i for i in range(12)]
df

plt.figure(figsize=(10,10))
df[1:].plot(marker='.')
plt.show()