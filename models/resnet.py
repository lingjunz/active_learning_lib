'''ResNet in PyTorch.

Reference:
https://github.com/kuangliu/pytorch-cifar
'''
import torch
import torch.nn as nn
import torch.nn.functional as F


class BasicBlock(nn.Module):
    expansion = 1
    def __init__(self, in_planes, planes, stride=1,dropRate=0.0):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.droprate = dropRate
        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate, training=True)
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        out = F.relu(out)
        return out

    def extract_features(self, x):
        feature_list = []
        out = F.relu(self.bn1(self.conv1(x)))
        feature_list.append(out)
        if self.droprate > 0:
            out =  F.dropout(out, p=self.droprate, training=True)
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        feature_list.append(out)

        return feature_list



class ResNet(nn.Module):
    def __init__(self, block, num_blocks, num_classes=10, dropRate = 0):
        super(ResNet, self).__init__()
        self.in_planes = 64

        self.conv1 = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1, dropRate=dropRate)
        self.layer2 = self._make_layer(block, 64, num_blocks[1], stride=2, dropRate=dropRate)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2, dropRate=dropRate)
        self.layer4 = self._make_layer(block, 128, num_blocks[3], stride=2, dropRate=dropRate)
        self.linear = nn.Linear(128*block.expansion, num_classes)
        self.dropout = dropRate
        self.block_num = len(num_blocks)
        self.layers = {1:self.layer1,2:self.layer2,3:self.layer3,4:self.layer4}


    def _make_layer(self, block, planes, num_blocks, stride,dropRate):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride, dropRate))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out1 = self.layer1(out)
        out2 = self.layer2(out1)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out = F.avg_pool2d(out4, 4)
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out, [out1, out2, out3, out4]

    def feature_list(self, x):
        name_list = []
        feature_list = []
        name_list.append("conv1")
        out = self.conv1(x)
        feature_list.append(out)
        out = F.relu(self.bn1(out))
        for idx in range(self.block_num):
            cur_idx = idx + 1
            cur_sequential = self.layers[cur_idx]
            for i,(name,module) in enumerate(cur_sequential._modules.items()):
                cur_features = module.extract_features(out)
                out = module(out)
                for j,each_features in enumerate(cur_features):
                    cur_name = "layer_{}_{}_{}".format(cur_idx,i,j)
                    name_list.append(cur_name)
                    feature_list.append(each_features)
            
        out = F.avg_pool2d(out, 4) # 4
        out = out.view(out.size(0), -1)
        logits = self.linear(out)
        name_list.append("logits")
        feature_list.append(logits)
        return logits, feature_list, name_list


def ResNet18(num_classes = 10,dropRate=0.0):
    return ResNet(BasicBlock, [2,2,2,2], num_classes,dropRate)

def ResNet34():
    return ResNet(BasicBlock, [3,4,6,3])


# class Bottleneck(nn.Module):
#     expansion = 4
#     def __init__(self, in_planes, planes, stride=1, dropRate=0.0):
#         super(Bottleneck, self).__init__()
#         self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, bias=False)
#         self.bn1 = nn.BatchNorm2d(planes)
#         self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
#         self.bn2 = nn.BatchNorm2d(planes)
#         self.conv3 = nn.Conv2d(planes, self.expansion*planes, kernel_size=1, bias=False)
#         self.bn3 = nn.BatchNorm2d(self.expansion*planes)
#         self.droprate = dropRate
#         self.shortcut = nn.Sequential()
#         if stride != 1 or in_planes != self.expansion*planes:
#             self.shortcut = nn.Sequential(
#                 nn.Conv2d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False),
#                 nn.BatchNorm2d(self.expansion*planes)
#             )

#     def forward(self, x):
#         out = F.relu(self.bn1(self.conv1(x)))
#         if self.droprate > 0:
#             out = F.dropout(out, p=self.droprate, training=True)
#         out = F.relu(self.bn2(self.conv2(out)))
#         if self.droprate > 0:
#             out = F.dropout(out, p=self.droprate, training=True)
#         out = self.bn3(self.conv3(out))
#         out += self.shortcut(x)
#         out = F.relu(out)
#         return out


# def ResNet50():
#     return ResNet(Bottleneck, [3,4,6,3])

# def ResNet101():
#     return ResNet(Bottleneck, [3,4,23,3])

# def ResNet152():
#     return ResNet(Bottleneck, [3,8,36,3])