import torch
import os
os.environ['CUDA_VISIBLE_DEVICES']='1'

from keras.datasets import cifar10
import numpy as np
import multiprocessing
from generate_utils import generate_adv




model_weights = {
    "resnet18_LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_1.pth",
    "resnet18_Entropy":"./cifar10/train/weights/active_resnet18_cifar10_Entropy_1.pth",
}

if __name__ == "__main__":
    (_,_),(images,labels) = cifar10.load_data()
    model_name = "resnet18_Entropy"
    model_path = model_weights[model_name]
    dataset = "cifar10"
    save_path = "adv/{}/{}/".format(dataset,model_name)
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    attack_lst=['fgsm','pgd','deepfool','cw','jsma' ]
    for attack in attack_lst:
        params = (images, labels, model_path, save_path, attack, dataset)
        generate_adv(*params)
        print("finish ",attack)



    # pool = multiprocessing.Pool(processes=8)
    # for attack in attack_lst:
        # params = (images, labels, model_path, save_path, attack, dataset)
        # print(attack)
        # pool.apply_async(generate_adv, params)
    # pool.close()
    # pool.join()



    