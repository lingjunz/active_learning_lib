
import torchvision.transforms as T
from keras.datasets import cifar10
from models.resnet import ResNet18
from torch.utils.data import DataLoader
from data.UserDataset import UserDataset
import torch
from generate_utils import generate_mutation,mtest


img_shape=(32,32)
batch_num=256
cifar10_mean = [0.4914, 0.4822, 0.4465]
cifar10_std = [0.247, 0.2435, 0.2616]
test_transform = T.Compose([
            T.ToTensor(),
            T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
        ])

model_weights = {
        "resnet18_LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_1.pth",
        "resnet18_Entropy":"./cifar10/train/weights/active_resnet18_cifar10_Entropy_1.pth",
    }
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '1'
import numpy as np
if __name__== "__main__":
    (_,_),(samples,labels) = cifar10.load_data()
    model_name = "resnet18_Entropy"

    dataset = "cifar10"
    save_path = "mutation/{}/".format(dataset)
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    
    net = ResNet18(num_classes=10).cuda()
    net.load_state_dict(torch.load(model_weights[model_name])['state_dict_backbone'])
    net.eval()

    

    mutated_samples,ground_truth = generate_mutation(samples,np.int64(labels.squeeze()), test_transform, net)

    np.save(os.path.join(save_path,"m_{}_{}_samples.npy".format(dataset,model_name)),mutated_samples)
    np.save(os.path.join(save_path,"m_{}_{}_labels.npy".format(dataset,model_name)),ground_truth)
    print("[{},{}]Save samples and labels successfully!".format(model_name,dataset))


    dataset_train = UserDataset(mutated_samples, img_shape, ground_truth, test_transform)
    dataloader  = DataLoader(dataset_train, batch_num, num_workers=4, )
    acc, c_conf, w_conf, right_index, wrong_index = mtest( net, dataloader )
    print("[Final] Acc: {}({}/{})\tC_conf:{}\tW_conf:{}".format(acc,len(right_index),len(mutated_samples),c_conf, w_conf))