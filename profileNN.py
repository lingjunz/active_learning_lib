import torch
import pickle
import torchvision.transforms as T
from torch.utils.data import DataLoader
import collections
import numpy as np
from keras.datasets import cifar10,mnist
import os 
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def profileDNN(net,dataloader):
    net.eval()
    cov_dict = collections.OrderedDict()
    profile_num = len(dataloader.dataset)
    device = torch.device('cuda:0')
    for i,(data,_) in enumerate(dataloader):
        data = data.to(device)
        _, out_list, name_list = net.feature_list(data)
        cur_batch = len(data)
        finished_num = i * dataloader.batch_size 
        
        if i == 0:
            neurons = [item.size(1) for item in out_list]
            print("layer_name",name_list)
            print("neuron_num",neurons)
            print("total:",np.sum(neurons))
            for temp in range(len(out_list)):
                print(out_list[temp].shape)
        if i % 100 == 0:
            print("* finish {}/{} samples".format(finished_num,profile_num))
            
        for layer_id in range(len(out_list)):
            cur_layer = name_list[layer_id]
            cur_neurons = out_list[layer_id].size(1)
            if len(out_list[layer_id].shape)==4:
                neurons_mean = to_np(out_list[layer_id]).mean(axis=(0,2,3)) # batch_num,cur_neurons
                neurons_max = to_np(out_list[layer_id]).mean(axis=(2,3)).max(axis=0)
                neurons_min = to_np(out_list[layer_id]).mean(axis=(2,3)).min(axis=0)
                neurons_square = np.sum((to_np(out_list[layer_id]).mean(axis=(2,3))**2),axis=0)
            else:
                neurons_mean = to_np(out_list[layer_id]).mean(axis=0) # batch_num,cur_neurons
                neurons_max = to_np(out_list[layer_id]).max(axis=0)
                neurons_min = to_np(out_list[layer_id]).min(axis=0)
                neurons_square = np.sum((to_np(out_list[layer_id])**2),axis=0)
                
                
            for neuron_id in range(cur_neurons):
                
                if (cur_layer,neuron_id) not in cov_dict:
                    # [mean_value_new, squared_mean_value, standard_deviation, lower_bound, upper_bound]
                    cov_dict[(cur_layer, neuron_id)] = [0.0, 0.0, 0.0, None, None]
                
                profile_data_list = cov_dict[(cur_layer, neuron_id)]
                mean_value = profile_data_list[0]
                squared_mean_value = profile_data_list[1]
                lower_bound = profile_data_list[3]
                upper_bound = profile_data_list[4]
                
                total_mean_value = mean_value * finished_num
                total_squared_mean_value = squared_mean_value * finished_num
                mean_value_new = (neurons_mean[neuron_id]*cur_batch + total_mean_value) / (finished_num + cur_batch)
                squared_mean_value =  (neurons_square[neuron_id] + total_squared_mean_value) / (finished_num + cur_batch)
                
                standard_deviation = np.math.sqrt(abs(squared_mean_value - mean_value_new * mean_value_new))
                if (lower_bound is None) and (upper_bound is None):
                        lower_bound = neurons_min[neuron_id]
                        upper_bound = neurons_max[neuron_id]
                else:
                    if neurons_min[neuron_id] < lower_bound:
                        lower_bound = neurons_min[neuron_id]

                    if neurons_max[neuron_id] > upper_bound:
                        upper_bound = neurons_max[neuron_id]
            
            
                profile_data_list[0] = mean_value_new
                profile_data_list[1] = squared_mean_value
                profile_data_list[2] = standard_deviation
                profile_data_list[3] = lower_bound
                profile_data_list[4] = upper_bound
                cov_dict[(cur_layer, neuron_id)] = profile_data_list    
                
    print(profile_num,finished_num+cur_batch)
    assert(profile_num==finished_num+cur_batch)
        
    return cov_dict

def dump(cov_dict, output_file):
    print("* profiling neuron size:", len(cov_dict.items()))
#     for item in cov_dict.items():
#         print(item)
    pickle_out = open(output_file, "wb")
    pickle.dump(cov_dict, pickle_out)
    pickle_out.close()

    print("write out profiling coverage results to ", output_file)
    print("done.")

def load_pickle(filepath):
    profile_dict = pickle.load(open(filepath, 'rb'))
    print("Load {} successfully!".format(filepath))
    print("* profiling neuron size:", len(profile_dict.items()))
    return profile_dict


import argparse
import os
import models.resnet as resnet
from data.UserDataset import UserDataset

model_weights = {
    "LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_1.pth",
    "Entropy":"./cifar10/train/weights/active_resnet18_cifar10_Entropy_1.pth",
}

# python ProfileNN.py -net lenet5 -oe 1 -o ../DeepEvolve/net_profiling -data_type mnist
if __name__=="__main__":
    torch.backends.cudnn.benchmark = True
    device = torch.device('cuda:0')
    parser = argparse.ArgumentParser(description="Profile Neural Network with Pytorch")
    parser.add_argument("-model_name",default="resnet18",help="model structure")
    parser.add_argument('-model_weights',choices=['LLAL','Entropy'],help="target model to be profiled")
    parser.add_argument('-oe',choices=[0,1],type=int,default=0,help="choose to profile oe model or not")
    parser.add_argument("-o",default="./cifar10/profiling",help="folder path to store profiling file")
    parser.add_argument("-data_type",choices=['cifar10','mnist'],default="cifar10",help="choose correct data type")
    args = parser.parse_args()
    
    model_name = args.model_name
    model_type = args.model_weights
    oe_model = args.oe
    output_path = args.o
    data_type = args.data_type
    if not os.path.exists(output_path):
        os.makedirs(output_path)
        
    if oe_model:
        model_name += "_oe"
    
    weights_path = model_weights[model_type]
    net    = resnet.ResNet18(num_classes=10).cuda()
    net.load_state_dict(torch.load(weights_path)['state_dict_backbone'])


    if data_type == "cifar10":
        
        cifar10_mean = [0.4914, 0.4822, 0.4465]
        cifar10_std = [0.247, 0.2435, 0.2616] # (0.2023, 0.1994, 0.2010)
        # Data
        train_transform = T.Compose([
            T.RandomHorizontalFlip(),
            T.RandomCrop(size=32, padding=4),
            T.ToTensor(),
            T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
        ])
        test_transform = T.Compose([
            T.ToTensor(),
            T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
        ])

        (x_train,y_train),(_,_) = cifar10.load_data()
        idx = np.load("./cifar10/train/weights/active_resnet18_cifar10_LLAL_1_idx.npy")[:10000]
        x_train,y_train = x_train[idx],y_train[idx]
        batch_num = 50
        img_size = (32,32)
        total_size = 10000

    dataset_train = UserDataset(x_train ,(32,32), labels = y_train, transform = test_transform)
    train_loader  = DataLoader(dataset_train, batch_size=batch_num, num_workers=4, pin_memory=True)

    
    cov_dict = profileDNN(net,train_loader)
    
    save_path = os.path.join(output_path,"{}_{}_0_{}".format(model_name,model_type,total_size))
    
    dump(cov_dict, save_path+".pickle")

    
    log = open(save_path+".log","w+")
    for key in cov_dict.keys():
        log.write("{}:{}\n".format(key,cov_dict[key]))
    log.flush()
    log.close()