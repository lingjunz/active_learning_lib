

import torch
import numpy as np


# Utils
import visdom
from tqdm import tqdm

import os
os.environ['CUDA_VISIBLE_DEVICES']='1'
from config import *


# Loss Prediction Loss
def LossPredLoss(input, target, margin=1.0, reduction='mean'):
    assert len(input) % 2 == 0, 'the batch size is not even.'
    assert input.shape == input.flip(0).shape
    input = (input - input.flip(0))[:len(input)//2] # [l_1 - l_2B, l_2 - l_2B-1, ... , l_B - l_B+1], where batch_size = 2B
    target = (target - target.flip(0))[:len(target)//2]
    target = target.detach()
    one = 2 * torch.sign(torch.clamp(target, min=0)) - 1 # 1 operation which is defined by the authors
    if reduction == 'mean':
        loss = torch.sum(torch.clamp(margin - one * input, min=0))
        loss = loss / input.size(0) # Note that the size of input is already halved
    elif reduction == 'none':
        loss = torch.clamp(margin - one * input, min=0)
    else:
        NotImplementedError()
    
    return loss

##
# Train Utils
# iters = 0
#
import models.resnet as resnet
import models.lossnet as lossnet

model_weights = {
    "cifar10":{
        "LLAL":"./cifar10/train/weights/active_resnet18_cifar10_LLAL_1.pth",
        "Entropy":"./cifar10/train/weights/active_resnet18_cifar10_Entropy_1.pth",
    }
    
}
def load_models(data_type,pretrained,strategy,train=False):
    # Load Model
    if data_type == "cifar10":
        net     = resnet.ResNet18(num_classes=10).cuda()
        loss_module = lossnet.LossNet().cuda()
        if not train:
            weights_path = model_weights[data_type][pretrained]
            net.load_state_dict(torch.load(weights_path)['state_dict_backbone'])

        if strategy == "VRO" and not train:
            net_dropout    = resnet.ResNet18(num_classes=10,dropRate=0.25).cuda()
            net_dropout.load_state_dict(torch.load(weights_path)['state_dict_backbone'])
        
        if pretrained == "LLAL" and not train:
            loss_module.load_state_dict(torch.load(weights_path)['state_dict_module'])
        
        if strategy == "VRO":
            models      = {'backbone': net, 'module': loss_module, 'dropout':net_dropout}
        else:
            models      = {'backbone': net, 'module': loss_module}

    return models

import torch.nn as nn
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler

def get_train_utils(strategy,models,lr=RE_LR,momentum=MOMENTUM,weight_decay=WDECAY,milestones=MILESTONES):
    # Loss, criterion and scheduler (re)initialization
    criterion      = nn.CrossEntropyLoss(reduction='none')
    optim_backbone = optim.SGD(models['backbone'].parameters(), lr=lr, 
                            momentum=momentum, weight_decay=weight_decay)
    sched_backbone = lr_scheduler.MultiStepLR(optim_backbone, milestones=milestones)
    if strategy == "LLAL":            
        optim_module   = optim.SGD(models['module'].parameters(), lr=lr, 
                                momentum=momentum, weight_decay=weight_decay)
        sched_module   = lr_scheduler.MultiStepLR(optim_module, milestones=milestones)
    else:
        optim_module, sched_module = None,None
    optimizers = {'backbone': optim_backbone, 'module': optim_module}
    schedulers = {'backbone': sched_backbone, 'module': sched_module}

    return criterion, optimizers, schedulers

def train_epoch(models, criterion, optimizers, dataloaders, epoch, epoch_loss, strategy):#, vis=None, plot_data=None):
    models['backbone'].train()
    if strategy == "LLAL":
        models['module'].train()
    # global iters
    correct = 0
    for data in tqdm(dataloaders['train'], leave=False, total=len(dataloaders['train'])):
        inputs = data[0].cuda()
        labels = data[1].cuda()
        # iters += 1
        optimizers['backbone'].zero_grad()
        if strategy == "LLAL":
            optimizers['module'].zero_grad()
        scores, features = models['backbone'](inputs)
        target_loss = criterion(scores, labels)
        pred = scores.data.max(1)[1]
        correct += pred.eq(labels.data).sum().item()
        if strategy == "LLAL":
            if epoch > epoch_loss:
                # After 120 epochs, stop the gradient from the loss prediction module propagated to the target model.
                features[0] = features[0].detach()
                features[1] = features[1].detach()
                features[2] = features[2].detach()
                features[3] = features[3].detach()
            pred_loss = models['module'](features)
            pred_loss = pred_loss.view(pred_loss.size(0))
            m_module_loss   = LossPredLoss(pred_loss, target_loss, margin=MARGIN)
        else:
            m_module_loss = 0
        m_backbone_loss = torch.sum(target_loss) / target_loss.size(0)
        loss            = m_backbone_loss + WEIGHT * m_module_loss
        loss.backward()
        optimizers['backbone'].step()
        if strategy == "LLAL":
            optimizers['module'].step()



def train(models, data_type ,criterion, optimizers, schedulers, dataloaders, num_epochs, epoch_loss,strategy):#, vis, plot_data):
    print('>> Train a Model.')
    # best_acc = 0.
    checkpoint_dir = os.path.join('./{}'.format(data_type), 'train', 'weights')
    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)
    
    for epoch in range(num_epochs):
        schedulers['backbone'].step()
        if strategy == "LLAL":
            schedulers['module'].step()
        train_epoch(models, criterion, optimizers, dataloaders, epoch, epoch_loss,strategy)#, vis, plot_data)

        
    print('>> Finished.')

def test( models, dataloaders, mode='val'):
    models['backbone'].eval()

    total = 0
    correct = 0
    right_conf = 0.0
    wrong_conf = 0.0
    with torch.no_grad():
        for (inputs, labels) in dataloaders[mode]:
            total += labels.size(0)
            inputs = inputs.cuda()
            labels = labels.cuda()

            scores, _ = models['backbone'](inputs)
            # msps, preds = torch.max(scores.data, 1)
            msps, preds = torch.max(torch.softmax(scores.data,1),1)
            correct += (preds == labels).sum().item()
            right_conf += msps[preds == labels].sum().item()
            wrong_conf += msps[preds != labels].sum().item()

    wrong = total - correct
    correct_avg = np.round(100 * right_conf / correct , 2) if correct != 0 else -1
    wrong_avg =np.round(100 * wrong_conf / wrong , 2) if wrong != 0 else -1
    return np.round(100 * correct / total, 2), correct_avg, wrong_avg

