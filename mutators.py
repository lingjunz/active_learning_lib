from __future__ import print_function
import sys
import cv2
import numpy as np
import random
import time
import copy


def image_translation(img, params):
    rows, cols, ch = img.shape
    M = np.float32([[1, 0, params], [0, 1, params]])
    dst = cv2.warpAffine(img.squeeze(), M, (cols, rows))
    return dst.reshape( rows, cols, ch)

def image_scale(img, params):

    # res = cv2.resize(img, None, fx=params[0], fy=params[1], interpolation=cv2.INTER_CUBIC)
    rows, cols, ch = img.shape

    res = cv2.resize(img.squeeze(), None, fx=params, fy=params, interpolation=cv2.INTER_CUBIC)
    res = res.reshape((res.shape[0],res.shape[1],ch))
    y, x, z = res.shape
    if params > 1:  # need to crop
        startx = int(x // 2 - cols // 2)
        starty = int(y // 2 - rows // 2)
        return res[starty:starty + rows, startx:startx + cols]
    elif params < 1:  # need to pad
        sty = int((rows - y) / 2)
        stx = int((cols - x) / 2)
        return np.pad(res, [(sty, rows - y - sty), (stx, cols - x - stx), (0, 0)], mode='constant',
                        constant_values=0)
    return res 

def image_shear(img, params):
    rows, cols, ch = img.shape
    factor = params * (-1.0)
    M = np.float32([[1, factor, 0], [0, 1, 0]])
    dst = cv2.warpAffine(img.squeeze(), M, (cols, rows))
    return dst.reshape( rows, cols, ch)

def image_rotation(img, params):
    rows, cols, ch = img.shape
    if ch == 1:
        img = img.squeeze()
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), params, 1)
    dst = cv2.warpAffine(img, M, (cols, rows), flags=cv2.INTER_AREA)
    return dst.reshape( rows, cols, ch)


def image_contrast(img, params):
    alpha = params
    width,height,ch = img.shape
    new_img = img.copy().squeeze()
    
    #imagenet patch_num = np.random.randint(5,20,1)[0]
    patch_num = np.random.randint(3,9,1)[0]
    x = np.random.randint(0,width-patch_num+1,1)[0]
    y = np.random.randint(0,height-patch_num+1,1)[0]
    
    new_img[x:x+patch_num,y:y+patch_num] = cv2.multiply(img[x:x+patch_num,y:y+patch_num], np.array([alpha]))  # mul_img = img*alpha
    new_img = np.clip(new_img,0,255)
    if new_img.shape != img.shape:
        new_img = new_img.reshape(width,height,ch)
    
    return new_img

def image_brightness(img, params):
    beta = params
    
    width,height,ch = img.shape
    
    new_img = img.copy().squeeze()
    #imagenet
    patch_num = np.random.randint(10,32,1)[0]
    x = np.random.randint(0,width-patch_num+1,1)[0]
    y = np.random.randint(0,height-patch_num+1,1)[0]
    

    new_img[x:x+patch_num,y:y+patch_num,] = cv2.add(img[x:x+patch_num,y:y+patch_num,], beta)  # new_img = img*alpha + beta
    
    if new_img.shape != img.shape:
        new_img = new_img.reshape(width,height,ch)
    
    return new_img

def image_blur(img, params):

    width,height,ch = img.shape

    blur = img.copy().squeeze()
    #imagenet
    patch_num = np.random.randint(3,9,1)[0]
    x = np.random.randint(0,width-patch_num+1,1)[0]
    y = np.random.randint(0,height-patch_num+1,1)[0]
    if params == 1:
        blur[x:x+patch_num,y:y+patch_num] = cv2.blur(blur[x:x+patch_num,y:y+patch_num,], (3,3))
    if params == 2:
        blur[x:x+patch_num,y:y+patch_num] = cv2.blur(blur[x:x+patch_num,y:y+patch_num,], (4,4))
    if params == 3:
        blur[x:x+patch_num,y:y+patch_num] = cv2.blur(blur[x:x+patch_num,y:y+patch_num,], (5,5))
    if params == 4:
        blur[x:x+patch_num,y:y+patch_num] = cv2.GaussianBlur(blur[x:x+patch_num,y:y+patch_num,], (3,3),0)
    if params == 5:
        blur[x:x+patch_num,y:y+patch_num] = cv2.GaussianBlur(blur[x:x+patch_num,y:y+patch_num,], (5,5),0)
    if params == 6:
        blur[x:x+patch_num,y:y+patch_num] = cv2.GaussianBlur(blur[x:x+patch_num,y:y+patch_num,], (7,7),0)
    if params == 7:
        blur[x:x+patch_num,y:y+patch_num,] = cv2.medianBlur(blur[x:x+patch_num,y:y+patch_num,],3)
    if params == 8:
        blur[x:x+patch_num,y:y+patch_num] = cv2.medianBlur(blur[x:x+patch_num,y:y+patch_num,],5)
    if params == 9:
        blur[x:x+patch_num,y:y+patch_num] = cv2.bilateralFilter(blur[x:x+patch_num,y:y+patch_num,],6,50,50)

    if blur.shape != img.shape:
        blur = blur.reshape(width,height,ch)
    return blur

def image_pixel_change(img, params):
    # random change 10 - 30 pixels from 0 -255
    img_shape = img.shape
    img1d = np.ravel(img.copy())
    index = np.random.choice(range(len(img1d)), params,replace=False)
    img1d[index] = np.random.randint(0, 256, params)
    new_img = img1d.reshape(img_shape)
    return new_img

def image_noise(img, params):
    assert np.max(img)>1
    params = 1
    if params == 1:  # Gaussian-distributed additive noise.
        row, col, ch = img.shape
        mean = 0
        var = 1
        sigma = var ** 0.5
        gauss = np.random.normal(mean, sigma, (row, col, ch))*10
        mask = np.random.uniform(0,1,(row, col, ch))
        noisy = np.clip(img + gauss,0,255)
        return noisy.astype(np.uint8)
    elif params == 2:  # Replaces random pixels with 0 or 1.
        s_vs_p = 0.5
        amount = 0.01 # 0.05
        out = np.copy(img)
        # Salt mode
        num_salt = np.ceil(amount * img.size * s_vs_p)
        coords = [np.random.randint(0, i, int(num_salt))
                    for i in img.shape]
        out[tuple(coords)] = 255

        # Pepper mode
        num_pepper = np.ceil(amount * img.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i, int(num_pepper))
                    for i in img.shape]
        out[tuple(coords)]= 0
        return out
    elif params == 3: # Multiplicative noise using out = image + n*image,where n is uniform noise with specified mean & variance.
        row, col, ch = img.shape
        gauss = np.random.randn(row, col, ch)
        mask = np.random.uniform(0,1,(row, col, ch))
        gauss *= mask
        noisy = np.clip(img + img * gauss,0,255)
        return noisy.astype(np.uint8)



# keras 1.2.2 tf:1.2.0
class Mutators():
     
    '''    
    TODO: Add more mutators, current version is from DeepTest, https://arxiv.org/pdf/1708.08559.pdf
    Also check,   https://arxiv.org/pdf/1712.01785.pdf, and DeepExplore
    '''
    # TODO: Random L 0
    # TODO: Random L infinity
    # more transformations refer to: http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_geometric_transformations/py_geometric_transformations.html#geometric-transformations
    # http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_table_of_contents_imgproc/py_table_of_contents_imgproc.html

    transformations = [image_translation,image_scale,image_shear,image_rotation,image_pixel_change, image_noise, image_contrast, image_brightness, image_blur]
    # these parameters need to be carefullly considered in the experiment
    # to consider the feedbacks
    params = []
    
    params.append(list(range(-3,3)))  # image_translation
    params.append([item*0.1 for item in range(8,13)])  # image_scale
    params.append([item*0.1 for item in range(-2,2)])  # image_shear
    params.append([item for item in range(-31,31,3)])  # image_rotation
    params.append(list(range(10, 30)))  # image_pixel_change
    params.append(list(range(1, 4)))  # image_noise
    
    
    params.append( [item*0.1 for item in range(8,13)] )  # image_contrast 6
    params.append( [item*0.1 for item in range(-51,51,5)])  # image_brightness 7
    params.append(list(range(1, 10)))  # image_blur

    
    @staticmethod
    def mutate(img, mutation_name, mutation_time ,mutation_type=0):
        mutation_dic = {"translation":0,"scale":1,"shear":2,"rotation":3,"pixel_change":4,"noise":5,"contrast":6,"brightness":7,"blur":8}
        if mutation_type == 0:
            used = [i for i in range(9)]
        elif mutation_type == 1:
            used = [mutation_dic[mutation_name]]
        else:
            print("mutation_id is wrong")
            sys.exit(0)
        random.seed(time.time())
        for i in range(mutation_time):
            tid = random.sample(used, 1)[0]
            transformation = Mutators.transformations[tid]
            params = Mutators.params[tid]
            param = random.sample(params, 1)[0]
            img_new = transformation(img, param)
            img = img_new.copy()

        return img