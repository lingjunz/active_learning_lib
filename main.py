'''Active Learning Procedure in PyTorch.

Reference:
[Yoo et al. 2019] Learning Loss for Active Learning (https://arxiv.org/abs/1905.03677)
'''
# Python
import os
import random
os.environ['CUDA_VISIBLE_DEVICES']='1'
# Torch
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import torch.optim.lr_scheduler as lr_scheduler
from torch.utils.data.sampler import SubsetRandomSampler

# Torchvison
import torchvision.models as models
# Utils
import visdom
# Custom
import models.resnet as resnet
import models.lossnet as lossnet
from config import *
from data.sampler import SubsetSequentialSampler
from data_utils import get_dataset,get_init_set,load_dataloaders
from train_utils import train,train_epoch,test,load_models,get_train_utils
from strategy import get_uncertainty,get_sorted_sequence
from collections import defaultdict

from data_utils import log_info



import argparse
## python main.py -strategy XENT 
# Main
if __name__ == '__main__':
    # vis = visdom.Visdom(server='http://localhost', port=9000)
    # plot_data = {'X': [], 'Y': [], 'legend': ['Backbone Loss', 'Module Loss', 'Total Loss']}

    parser = argparse.ArgumentParser(description="Active learning")
    parser.add_argument("-strategy",choices=["LLAL","Random","LeastConfidence","Margin","Entropy","XENT","GINI","LSA","DSA"],default="LLAL")
    parser.add_argument("-pretrained",choices=['LLAL','Entropy'],help="pretrained model",default="LLAL")
    parser.add_argument("-data_type",choices=['cifar10'],help="target classification task",default='cifar10')
    parser.add_argument("-model_type",help="model structure",default='resnet18',choices=['resnet18'])
    args = parser.parse_args()
    strategy = args.strategy
    pretrained = args.pretrained
    data_type = args.data_type
    model_type = args.model_type
    model_name = "{}_{}".format(model_type,strategy)
    logfile = open("{}_{}.log".format(data_type,strategy),"w+")
    results_dic = defaultdict()
    dataset_train, dataset_unlabeled, dataset_test = get_dataset(data_type)
    torch.backends.cudnn.benchmark = True
    for trial in range(TRIALS):
        # Initialize a labeled dataset by randomly sampling K=ADDENDUM=1,000 data points from the entire dataset.
        indices = list(range(NUM_TRAIN))
        labeled_set, unlabeled_set = get_init_set(data_type,ADDENDUM)
        np.save("./{}/train/weights/active_{}_{}_{}_temp_idx.npy".format(data_type,model_type,data_type,strategy),np.array(labeled_set))
        # Make dataloaders
        dataloaders = load_dataloaders( dataset_train, dataset_unlabeled, dataset_test,
                                    labeled_set, unlabeled_set)
        models = load_models(data_type,pretrained,strategy,train=True)
        # Active learning cycles
        for cycle in range(CYCLES):
            results_dic["epoch_"+str(cycle)] = defaultdict()
            # Loss, criterion and scheduler (re)initialization
            criterion, optimizers, schedulers = get_train_utils( strategy,models, lr=LR,momentum=MOMENTUM,
                                                                 weight_decay=WDECAY,milestones=MILESTONES)
            # Training and test
            train(models, data_type , criterion, optimizers, schedulers, dataloaders, EPOCH, EPOCHL,strategy)
            log_info(results_dic,logfile, cycle, CYCLES, len(labeled_set), models, dataloaders,mode="train")
            log_info(results_dic,logfile, cycle, CYCLES, len(dataset_train), models, dataloaders,mode="val")
            log_info(results_dic,logfile, cycle, CYCLES, len(dataset_test), models, dataloaders,mode="test")

            #  Update the labeled dataset via loss prediction-based uncertainty measurement
            # Randomly sample 10000 unlabeled data points
            random.shuffle(unlabeled_set)
            subset = unlabeled_set[:SUBSET]
            # Create unlabeled dataloader for the unlabeled subset
            unlabeled_loader = DataLoader(dataset_unlabeled, batch_size=BATCH, 
                                          sampler=SubsetSequentialSampler(subset), # more convenient if we maintain the order of subset
                                          pin_memory=True)
            
            # Measure uncertainty of each data points in the subset
            arg = get_sorted_sequence(strategy,models,unlabeled_loader,model_name)

            # Update the labeled dataset and the unlabeled dataset, respectively
            labeled_set += list(torch.tensor(subset)[arg][-ADDENDUM:].numpy()) # choose the most uncertainty data points
            unlabeled_set = list(torch.tensor(subset)[arg][:-ADDENDUM].numpy()) + unlabeled_set[SUBSET:]
            # Create a new dataloader for the updated labeled dataset
            dataloaders['train'] = DataLoader(dataset_train, batch_size=BATCH, 
                                              sampler=SubsetRandomSampler(labeled_set), 
                                              pin_memory=True)

            np.save("./{}/train/weights/active_{}_{}_{}_temp_idx.npy".format(data_type,model_type,data_type,strategy),np.array(labeled_set))
                                              
        if strategy == "LLAL":
            module_weights = models['module'].state_dict()
        else:
            module_weights = None
        # Save a checkpoint
        torch.save({
                    'trial': trial + 1,
                    'state_dict_backbone': models['backbone'].state_dict(),
                    'state_dict_module': module_weights
                },
                './{}/train/weights/active_{}_{}_{}_{}.pth'.format(data_type,model_type,data_type,strategy,trial+1))
        np.save("./{}/train/weights/active_{}_{}_{}_{}_idx.npy".format( data_type,
                                                                        model_type,
                                                                        data_type,
                                                                        strategy,trial+1),np.array(labeled_set[:ADDENDUM*CYCLES]))
        import joblib
        joblib.dump(results_dic,"./{}/train/weights/active_{}_{}_{}_{}_res.dic".format(data_type,model_type,data_type,strategy,trial+1))

    logfile.close()