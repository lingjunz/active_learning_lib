
import numpy as np

import torchvision.transforms as T
from torchvision.datasets import CIFAR100, CIFAR10
from data.UserDataset import UserDataset
from train_utils import test
from keras.datasets import cifar10
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import DataLoader
from config import *
import torch



def make_train_loader(idx_path):

    cifar10_mean = [0.4914, 0.4822, 0.4465]
    cifar10_std = [0.247, 0.2435, 0.2616]
    test_transform = T.Compose([
                T.ToTensor(),
                T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
            ])
    (x_train,y_train),(_,_) = cifar10.load_data()
    idx = np.load(idx_path)
    x_train,y_train = x_train[idx],y_train[idx].squeeze()
    batch_num = 256
    dataset_train = UserDataset(x_train ,(32,32), labels = y_train, transform = test_transform)
    train_loader  = DataLoader(dataset_train, batch_size=batch_num, num_workers=4, pin_memory=True)
    return train_loader,idx,y_train



def load_dataloaders(dataset_train, dataset_val, dataset_test, ini_labeled_set, ini_unlabeled_set ,batch_num=BATCH):
    # Make dataloaders
    train_loader = DataLoader(  dataset_train, batch_size=batch_num, 
                                sampler=SubsetRandomSampler(ini_labeled_set),
                                num_workers=4, pin_memory=True)
    test_loader  = DataLoader(dataset_test, batch_size=batch_num)
    val_loader = DataLoader(dataset_val, batch_size=batch_num,sampler=SubsetRandomSampler(ini_unlabeled_set),num_workers=4)
    dataloaders  = {'train': train_loader, 'val': val_loader,'test': test_loader}
    return dataloaders

def log_info(results_dic,logfile, cycle, cycles, size, models, dataloaders,mode):
    acc,correct_conf,wrong_conf = test(models, dataloaders, mode=mode)
    results_dic["epoch_"+str(cycle)][mode] = [acc,correct_conf,wrong_conf]
    print('[{}] Cycle {}/{} || Label set size {} Train acc {} correct_conf {} wrong_conf {}'.format(mode,
        cycle, cycles, size, acc,correct_conf,wrong_conf))
    logfile.write(
        '[{}] || Cycle {}/{} || Label set size {} Train acc {} correct_conf {} wrong_conf {}\n'.format(mode,
            cycle, cycles, size, acc,correct_conf,wrong_conf)
    )
    logfile.flush()

def get_init_set(name,sample_num):
    if name == "cifar10":
        (_,y_train),(_,_) = cifar10.load_data()
    labeled_set = []
    unlabeled_set = []
    for i in range(10):
        cur_idx = list(np.where(y_train.squeeze()==i)[0])
        cur_set = list(np.random.choice(cur_idx,int(sample_num/10),replace=False))
        labeled_set += cur_set
        unlabeled_set += [i for i in cur_idx if i not in cur_set]
    return labeled_set, unlabeled_set



def get_dataset(name, type="benign"):
    if name == "cifar10":
        cifar10_mean = [0.4914, 0.4822, 0.4465]
        cifar10_std = [0.247, 0.2435, 0.2616] # (0.2023, 0.1994, 0.2010)
        # Data
        train_transform = T.Compose([
            T.RandomHorizontalFlip(),
            T.RandomCrop(size=32, padding=4),
            T.ToTensor(),
            T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
        ])
        test_transform = T.Compose([
            T.ToTensor(),
            T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
        ])

        if type == "a":
            samples = np.load("adv_samples_LLAL.npy")
            idx = np.load("./cifar10/train/weights/active_resnet18_cifar10_LLAL_1_idx.npy")[:10000]
            (x_train, labels),(_,_) = cifar10.load_data()
            samples = np.concatenate((x_train[idx],samples),axis=0)
            labels = np.int64(labels[idx]).squeeze()
            a_labels = np.load("adv_labels_LLAL.npy")

            labels = np.concatenate((labels,a_labels),axis=0)
            dataset_train = UserDataset(samples ,(32,32), labels = labels, transform = train_transform)
            dataset_unlabeled = UserDataset(samples ,(32,32), labels = labels, transform = test_transform)
        elif type == "m":
            samples = np.load("m_samples.npy")
            idx = np.load("./cifar10/train/weights/active_resnet18_cifar10_LLAL_1_idx.npy")[:10000]
            (x_train, labels),(_,_) = cifar10.load_data()
            samples = np.concatenate((x_train[idx],samples),axis=0) 
            labels = np.int64(labels[idx]).squeeze()
            m_labels = np.load("m_labels.npy")
            labels = np.concatenate((labels,m_labels),axis=0)
            dataset_train = UserDataset(samples ,(32,32), labels = labels, transform = train_transform)
            dataset_unlabeled = UserDataset(samples ,(32,32), labels = labels, transform = test_transform)
        else:
            dataset_train = CIFAR10('/home/lingjun/ood_2020/data/pytorch_data/cifar10', train=True, transform=train_transform)
            dataset_unlabeled   = CIFAR10('/home/lingjun/ood_2020/data/pytorch_data/cifar10', train=True,  transform=test_transform)
        dataset_test  = CIFAR10('/home/lingjun/ood_2020/data/pytorch_data/cifar10', train=False, transform=test_transform)

    return dataset_train, dataset_unlabeled, dataset_test

