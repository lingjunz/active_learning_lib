
# Python
import os
import random
import argparse
import joblib
os.environ['CUDA_VISIBLE_DEVICES']='1'
# Torch
import torch
import numpy as np

# Torchvison
import torchvision.models as models
from torch.utils.data import DataLoader
# Custom

from config import *
from data.sampler import SubsetSequentialSampler
from data_utils import get_dataset,get_init_set,log_info,load_dataloaders
from train_utils import train,train_epoch,test, get_train_utils, load_models
from strategy import get_uncertainty,get_sorted_sequence
from collections import defaultdict




##
# Main
if __name__ == '__main__':
    # vis = visdom.Visdom(server='http://localhost', port=9000)
    # plot_data = {'X': [], 'Y': [], 'legend': ['Backbone Loss', 'Module Loss', 'Total Loss']}
    # model_name = 'resnet18_LLAL'
    torch.backends.cudnn.benchmark = True
    parser = argparse.ArgumentParser(description="Active learning")
    parser.add_argument("-strategy",choices=["LLAL","Random","LeastConfidence","Margin","Entropy","GINI","XENT","VRO","LSA","DSA"],default="LLAL")
    parser.add_argument("-pretrained",choices=['LLAL','Entropy'],help="pretrained model",default="LLAL")
    parser.add_argument("-sample_type",choices=['a','m','c'],help="adversarial examples or mutation samples or clean samples")
    parser.add_argument("-data_type",choices=['cifar10'],help="target classification task",default='cifar10')
    parser.add_argument("-model_type",help="model structure",default='resnet18',choices=['resnet18'])
    args = parser.parse_args()
    strategy = args.strategy
    pretrained = args.pretrained
    data_type = args.data_type
    sample_type = args.sample_type
    model_type = args.model_type
    model_name = "{}_{}".format(model_type,pretrained)
    results_dic = defaultdict()


    logfile = open("./{}/train/retrain/active_{}_{}_{}_{}.log".format(data_type,model_type,data_type,strategy,sample_type),"w+")
    dataset_train, dataset_val, dataset_test = get_dataset(data_type,sample_type)
    # Initial labeled set and unlabeled set
    ini_labeled_set = list(range(INI_NUM))
    ini_unlabeled_set = list(range(INI_NUM,INI_NUM + CRASH_NUM))
    # load dataloaders and models
    dataloaders = load_dataloaders( dataset_train, dataset_val, dataset_test,
                                    ini_labeled_set, ini_unlabeled_set)
    models = load_models(data_type,pretrained,strategy)
    # log stats
    cycle = 0
    results_dic["epoch_"+str(cycle)] = defaultdict()
    log_info(results_dic,logfile, cycle, CYCLES, INI_NUM, models, dataloaders,mode="train")
    log_info(results_dic,logfile, cycle, CYCLES, CRASH_NUM, models, dataloaders,mode="val")
    log_info(results_dic,logfile, cycle, CYCLES, len(dataset_test), models, dataloaders,mode="test")
    # Create unlabeled dataloader for the unlabeled subset
    np.random.shuffle(ini_unlabeled_set)
    unlabeled_loader = DataLoader( dataset_val, batch_size=BATCH, sampler=SubsetSequentialSampler(ini_unlabeled_set), 
                                    pin_memory=True)
    # Measure uncertainty of each data points in the subset
    arg = INI_NUM + get_sorted_sequence(strategy,models,unlabeled_loader,model_name)
    print(len(arg),torch.max(arg),torch.min(arg))
    np.save("./{}/train/retrain/active_{}_{}_{}_{}_idx.npy".format(data_type,model_type,data_type,strategy,sample_type),arg)
    # Active learning cycles
    for cycle in range(1,ADV_CYCLES+1):
        # choose the most promising data points
        labeled_set = ini_labeled_set + list(arg[-ADDENDUM*cycle:].numpy()) 
        # Create a new dataloader for the updated labeled dataset
        dataloaders['train'] = DataLoader(  dataset_train, batch_size=BATCH, 
                                            sampler=SubsetRandomSampler(labeled_set), 
                                            pin_memory=True)
        results_dic["epoch_"+str(cycle)] = defaultdict()
        # Loss, criterion and scheduler (re)initialization
        criterion, optimizers, schedulers = get_train_utils(strategy,models)
        # Training and test
        train(models, data_type ,criterion, optimizers, schedulers, dataloaders, RE_EPOCH, EPOCHL,strategy)
        # log stats
        log_info(results_dic,logfile, cycle, CYCLES, len(labeled_set), models, dataloaders,mode="train")
        log_info(results_dic,logfile, cycle, CYCLES, CRASH_NUM, models, dataloaders,mode="val")
        log_info(results_dic,logfile, cycle, CYCLES, len(dataset_test), models, dataloaders,mode="test")
        # reload the pre-trained model
        models = load_models(data_type,pretrained,strategy)
    # retrain with all mutation/adversarial examples
    cycle += 1
    labeled_set = ini_labeled_set + list(arg.numpy()) # choose the most uncertainty data points
    # Create a new dataloader for the updated labeled dataset
    dataloaders['train'] = DataLoader(  dataset_train, batch_size=BATCH, 
                                        sampler=SubsetRandomSampler(labeled_set), 
                                        pin_memory=True)
    results_dic["epoch_"+str(cycle)] = defaultdict()
    # Loss, criterion and scheduler (re)initialization
    criterion, optimizers, schedulers = get_train_utils(strategy,models)
    # Training and test
    train(models, data_type, criterion, optimizers, schedulers, dataloaders, RE_EPOCH, EPOCHL,strategy)
    # log stats
    log_info(results_dic,logfile, cycle, CYCLES, len(labeled_set), models, dataloaders,mode="train")
    log_info(results_dic,logfile, cycle, CYCLES, CRASH_NUM, models, dataloaders,mode="val")
    log_info(results_dic,logfile, cycle, CYCLES, len(dataset_test), models, dataloaders,mode="test")
    joblib.dump(results_dic,"./{}/train/retrain/active_{}_{}_{}_res_{}.dic".format(data_type,model_type,data_type,strategy,sample_type))
    logfile.close()
