# Python
import os
import random
import argparse
import joblib
os.environ['CUDA_VISIBLE_DEVICES']='1'
# Torch
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import torch.optim.lr_scheduler as lr_scheduler
from torch.utils.data.sampler import SubsetRandomSampler

# Torchvison
import torchvision.models as models
from keras.datasets import cifar10
from torchvision.datasets import CIFAR100, CIFAR10
# Custom
import models.resnet as resnet
import models.lossnet as lossnet
from config import *


from train_utils import train,train_epoch,test
from strategy import get_uncertainty,get_sorted_sequence
from collections import defaultdict
from data.UserDataset import UserDataset
import torchvision.transforms as T
from tqdm import tqdm


def train(models, criterion, optimizers, schedulers, dataloaders, num_epochs, epoch_loss,strategy):#, vis, plot_data):
    print('>> Train a Model.')
    # best_acc = 0.
    checkpoint_dir = os.path.join('./cifar10', 'train', 'weights')
    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)
    
    for epoch in range(num_epochs):
        schedulers['backbone'].step()
        train_epoch(models, criterion, optimizers, dataloaders, epoch, epoch_loss,strategy)#, vis, plot_data)


        
    print('>> Finished.')


def test( models, dataloaders, mode='val'):
    assert mode in ['val','test','train']
    models['backbone'].eval()


    total = 0
    correct = 0
    right_conf = 0.0
    wrong_conf = 0.0
    with torch.no_grad():
        for (inputs, labels) in dataloaders[mode]:
            total += labels.size(0)
            inputs = inputs.cuda()
            labels = labels.cuda()

            scores, _ = models['backbone'](inputs)
            # msps, preds = torch.max(scores.data, 1)
            msps, preds = torch.max(torch.softmax(scores.data,1),1)
            correct += (preds == labels).sum().item()
            right_conf += msps[preds == labels].sum().item()
            wrong_conf += msps[preds != labels].sum().item()

    wrong = total - correct
    correct_avg = np.round(100 * right_conf / correct , 2) if correct != 0 else -1
    wrong_avg =np.round(100 * wrong_conf / wrong , 2) if wrong != 0 else -1
    return np.round(100 * correct / total, 2), correct_avg, wrong_avg



if __name__ == '__main__':
    
    data_type = "cifar10"

    torch.backends.cudnn.benchmark = True
    strategy = None
   
    cifar10_mean = [0.4914, 0.4822, 0.4465]
    cifar10_std = [0.247, 0.2435, 0.2616] # (0.2023, 0.1994, 0.2010)
    # Data
    train_transform = T.Compose([
        T.RandomHorizontalFlip(),
        T.RandomCrop(size=32, padding=4),
        T.ToTensor(),
        T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
    ])

    test_transform = T.Compose([
            T.ToTensor(),
            T.Normalize(cifar10_mean, cifar10_std) # T.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761)) # CIFAR-100
        ])

    dataset_train = CIFAR10('/home/lingjun/ood_2020/data/pytorch_data/cifar10', train=True, transform=train_transform)
    dataset_test  = CIFAR10('/home/lingjun/ood_2020/data/pytorch_data/cifar10', train=False, transform=test_transform)

    train_loader = DataLoader(dataset_train, batch_size=BATCH, num_workers=4, pin_memory=True)
    test_loader  = DataLoader(dataset_test, batch_size=BATCH, num_workers=4, pin_memory=True)
    dataloaders  = {'train': train_loader, 'test': test_loader}

    resnet18    = resnet.ResNet18(num_classes=10).cuda()
    loss_module = None
    models      = {'backbone': resnet18, 'module': loss_module}

    # Loss, criterion and scheduler (re)initialization
    criterion      = nn.CrossEntropyLoss(reduction='none')
    optim_backbone = optim.SGD(models['backbone'].parameters(), lr=RE_LR, 
                            momentum=MOMENTUM, weight_decay=WDECAY)
    sched_backbone = lr_scheduler.MultiStepLR(optim_backbone, milestones=MILESTONES)
    
    optim_module, sched_module = None, None
    optimizers = {'backbone': optim_backbone, 'module': optim_module}
    schedulers = {'backbone': sched_backbone, 'module': sched_module}

    # Training and test
    train(models, criterion, optimizers, schedulers, dataloaders, 200, EPOCHL, strategy)#, vis, plot_data)

    acc,correct_conf,wrong_conf = test(models, dataloaders, mode="test")

     # Save a checkpoint
    torch.save({
                # 'trial': trial + 1,
                'state_dict_backbone': models['backbone'].state_dict(),
            },
            './cifar10/train/weights/resnet18_cifar10_normal_{}.pth'.format(acc))
    # print(acc, correct_conf, wrong_conf)



'''
LLAL all
[Train]acc:41.38,correct:88.29,wrong_conf:79.81
[Val  ]acc:0.0,correct:-1,wrong_conf:77.84
[Test ]acc:90.14,correct:97.86,wrong_conf:81.5
>> Train a Model.
[Train]epoch:0,acc:64.83,correct:82.57,wrong_conf:65.74
[Val  ]epoch:0,acc:62.14,correct:82.47,wrong_conf:67.14
[Test ]epoch:0,acc:79.09,correct:90.3,wrong_conf:69.96
[Train]epoch:4,acc:69.79,correct:86.1,wrong_conf:67.8
[Val  ]epoch:4,acc:69.03,correct:86.96,wrong_conf:69.3
[Test ]epoch:4,acc:80.5,correct:92.44,wrong_conf:72.43
[Train]epoch:9,acc:71.94,correct:87.94,wrong_conf:69.68
[Val  ]epoch:9,acc:71.93,correct:88.61,wrong_conf:71.07
[Test ]epoch:9,acc:79.88,correct:92.88,wrong_conf:74.41
[Train]epoch:14,acc:72.58,correct:88.33,wrong_conf:70.14
[Val  ]epoch:14,acc:72.08,correct:89.17,wrong_conf:71.79
[Test ]epoch:14,acc:79.49,correct:93.71,wrong_conf:76.51
[Train]epoch:19,acc:78.58,correct:88.52,wrong_conf:67.4
[Val  ]epoch:19,acc:79.02,correct:89.56,wrong_conf:69.05
[Test ]epoch:19,acc:82.21,correct:93.74,wrong_conf:74.5
[Train]epoch:24,acc:76.13,correct:89.5,wrong_conf:69.52
[Val  ]epoch:24,acc:76.75,correct:90.37,wrong_conf:70.85
[Test ]epoch:24,acc:80.93,correct:94.09,wrong_conf:75.93
[Train]epoch:29,acc:75.11,correct:89.5,wrong_conf:71.69
[Val  ]epoch:29,acc:75.66,correct:90.52,wrong_conf:73.05
[Test ]epoch:29,acc:77.05,correct:93.7,wrong_conf:78.74
[Train]epoch:34,acc:72.5,correct:89.17,wrong_conf:71.13
[Val  ]epoch:34,acc:73.26,correct:89.91,wrong_conf:72.73
[Test ]epoch:34,acc:78.69,correct:93.26,wrong_conf:75.74
[Train]epoch:39,acc:79.02,correct:90.18,wrong_conf:68.97
[Val  ]epoch:39,acc:80.18,correct:91.12,wrong_conf:70.11
[Test ]epoch:39,acc:82.34,correct:94.31,wrong_conf:74.55
[Train]epoch:44,acc:76.08,correct:89.73,wrong_conf:70.69
[Val  ]epoch:44,acc:77.26,correct:90.71,wrong_conf:72.1
[Test ]epoch:44,acc:79.79,correct:94.01,wrong_conf:76.52
[Train]epoch:49,acc:78.07,correct:90.06,wrong_conf:69.64
[Val  ]epoch:49,acc:79.31,correct:90.94,wrong_conf:70.6
[Test ]epoch:49,acc:81.31,correct:94.24,wrong_conf:76.97
>> Finished.



Entropy all
Using TensorFlow backend.
[Train]acc:40.28,correct:87.62,wrong_conf:79.28
[Val  ]acc:0.0,correct:-1,wrong_conf:76.39
[Test ]acc:89.17,correct:97.66,wrong_conf:81.0
>> Train a Model.
[Train]epoch:0,acc:71.74,correct:85.46,wrong_conf:66.93
[Val  ]epoch:0,acc:68.42,correct:84.5,wrong_conf:67.88
[Test ]epoch:0,acc:82.71,correct:92.64,wrong_conf:72.63
[Train]epoch:4,acc:82.09,correct:90.23,wrong_conf:69.05
[Val  ]epoch:4,acc:80.69,correct:89.72,wrong_conf:69.14
[Test ]epoch:4,acc:86.55,correct:94.48,wrong_conf:74.04
[Train]epoch:9,acc:86.28,correct:92.9,wrong_conf:71.21
[Val  ]epoch:9,acc:85.66,correct:92.68,wrong_conf:71.64
[Test ]epoch:9,acc:87.49,correct:95.63,wrong_conf:76.96
[Train]epoch:14,acc:88.71,correct:94.48,wrong_conf:73.19
[Val  ]epoch:14,acc:88.65,correct:94.33,wrong_conf:73.91
[Test ]epoch:14,acc:87.16,correct:96.56,wrong_conf:79.53
[Train]epoch:19,acc:90.24,correct:95.61,wrong_conf:75.42
[Val  ]epoch:19,acc:90.3,correct:95.63,wrong_conf:75.58
[Test ]epoch:19,acc:87.35,correct:96.95,wrong_conf:80.82
[Train]epoch:24,acc:91.72,correct:96.27,wrong_conf:75.24
[Val  ]epoch:24,acc:91.98,correct:96.28,wrong_conf:75.86
[Test ]epoch:24,acc:87.77,correct:97.25,wrong_conf:80.64
[Train]epoch:29,acc:91.34,correct:96.36,wrong_conf:76.27
[Val  ]epoch:29,acc:91.77,correct:96.51,wrong_conf:76.7
[Test ]epoch:29,acc:87.18,correct:97.17,wrong_conf:81.07
[Train]epoch:34,acc:92.69,correct:96.83,wrong_conf:76.76
[Val  ]epoch:34,acc:93.06,correct:96.8,wrong_conf:76.78
[Test ]epoch:34,acc:87.02,correct:97.32,wrong_conf:81.72
[Train]epoch:39,acc:91.92,correct:96.74,wrong_conf:76.52
[Val  ]epoch:39,acc:92.43,correct:96.82,wrong_conf:77.06
[Test ]epoch:39,acc:86.92,correct:97.21,wrong_conf:82.41
[Train]epoch:44,acc:90.92,correct:96.64,wrong_conf:77.99
[Val  ]epoch:44,acc:91.59,correct:96.87,wrong_conf:78.24
[Test ]epoch:44,acc:85.86,correct:97.31,wrong_conf:82.37
[Train]epoch:49,acc:92.25,correct:96.78,wrong_conf:77.54
[Val  ]epoch:49,acc:92.59,correct:96.91,wrong_conf:77.66
[Test ]epoch:49,acc:86.36,correct:97.34,wrong_conf:81.36
>> Finished.

'''